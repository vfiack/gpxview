package fr.vfiack.gpxview;

import org.jcodec.common.DictionaryCompressor;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

public class SequencerTest {
    @Test
    public void testEmpty() {
        Sequencer<Object> sequencer = new Sequencer<>();
        List<Object> empty = Collections.emptyList();
        assertTrue("empty list should remain empty", sequencer.sequence(empty).isEmpty());
    }

    @Test
    public void testSmallStrings() {
        Sequencer<String> sequencer = new Sequencer<>();
        List<String> list = Arrays.asList("a", "b", "c");
        List<String> sequence = sequencer.sequence(list);
        Assert.assertEquals("a", sequence.get(0));
        Assert.assertEquals("c", sequence.get(1));
        Assert.assertEquals("b", sequence.get(2));
    }

    @Test
    public void testSequence() {
        Sequencer<Integer> sequencer = new Sequencer<>();
        List<Integer> list = Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
        List<Integer> expected = Arrays.asList(0, 9, 5, 3, 7, 2, 4, 6, 8, 1);
        Assert.assertEquals(expected, sequencer.sequence(list));
    }
}

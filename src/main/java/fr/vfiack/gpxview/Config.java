package fr.vfiack.gpxview;

public class Config {
    private final int streetviewWidth = 640;
    private final int streetviewHeight = 320;
    private final String steetviewFov = "120";

    private final int mapWidth = 640;
    private final int mapHeight = 320;
    private final String mapZoom = "14";
    private final String mapType = "hybrid"; //road, terrain, satellite, hybrid

    public String getStreetviewSize() {
        return streetviewWidth + "x" + streetviewHeight;
    }

    public String getMapSize() {
        return mapWidth + "x" + mapHeight;
    }

    //--

    public String getSteetviewFov() {
        return steetviewFov;
    }

    public String getMapZoom() {
        return mapZoom;
    }

    public String getMapType() {
        return mapType;
    }

    public int getStreetviewWidth() {
        return streetviewWidth;
    }

    public int getStreetviewHeight() {
        return streetviewHeight;
    }

    public int getMapWidth() {
        return mapWidth;
    }

    public int getMapHeight() {
        return mapHeight;
    }
}

package fr.vfiack.gpxview;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;


public class Downloader {

	private final static String STREETVIEW_URL = "https://maps.googleapis.com/maps/api/streetview";
    private final static String MAPS_URL = "https://maps.googleapis.com/maps/api/staticmap";

    private final Config config;
    private final String apiKey;

    public Downloader(Config config, String apiKey) {
        this.config = config;
        this.apiKey = apiKey;
    }

    public BufferedImage downloadStreetView(String location, String pitch, String heading) throws IOException {
        URL streetviewUrl = new URL(STREETVIEW_URL + "?key=" + apiKey
                + "&size=" + config.getStreetviewSize() + "&fov=" + config.getSteetviewFov() + "&pitch=" + pitch
                + "&heading=" + heading + "&location=" + location);

        return ImageIO.read(streetviewUrl.openStream());
    }

    public BufferedImage downloadMapImage(String location, String path) throws IOException {
        URL mapsUrl = new URL(MAPS_URL + "?key=" + apiKey
                + "&size=" + config.getMapSize() + "&path=" + path
                + "&markers=" + location + "&center=" + location
                + "&zoom=" + config.getMapZoom() + "&maptype=" + config.getMapType());

        return ImageIO.read(mapsUrl.openStream());
    }
}

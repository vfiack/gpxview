package fr.vfiack.gpxview;

public class TrackPoint {
	private final int index;
	private final String latitude;
	private final String longitude;
	private final String elevation;
	private final String time;
		
	public TrackPoint(int index, String latitude, String longitude, String elevation, String time) {
		this.index = index;
		this.latitude = latitude;
		this.longitude = longitude;
		this.elevation = elevation;
		this.time = time;
	}
	
	//--

	public String getLongitude() {
		return longitude;
	}
	public String getElevation() {
		return elevation;
	}
    public String getLatitude() {
		return latitude;
	}
	public int getIndex() {
		return index;
	}

	//--
	
	@Override
	public String toString() {
		return "TrackPoint [latitude=" + latitude + ", longitude=" + longitude
				+ ", elevation=" + elevation + ", time=" + time + "]";
	}	
	
}

package fr.vfiack.gpxview;

import java.awt.image.BufferedImage;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class ImageContainer {
    private final TrackPoint trackPoint;
    private final Future<BufferedImage> image;

    public ImageContainer(TrackPoint trackPoint, Future<BufferedImage> image) {
        this.trackPoint = trackPoint;
        this.image = image;
    }

    public BufferedImage getImage() {
        try {
            return image.get();
        } catch (InterruptedException|ExecutionException e) {
            throw new RuntimeException(e);
        }
    }

    public TrackPoint getTrackPoint() {
        return trackPoint;
    }
}

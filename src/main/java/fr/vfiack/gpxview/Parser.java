package fr.vfiack.gpxview;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Parser {
	public List<TrackPoint> parseGpx(File gpxFile) throws ParserConfigurationException, SAXException, IOException, XPathExpressionException {
		List<TrackPoint> result = new ArrayList<>();

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document doc = builder.parse(gpxFile);

		XPathFactory xFactory = XPathFactory.newInstance();
		XPath xpath = xFactory.newXPath();
		XPathExpression expr = xpath.compile("//trkpt");

		NodeList nodes = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);

		for (int n = 0; n < nodes.getLength(); n++) {
			Node node = nodes.item(n);
			String lat = node.getAttributes().getNamedItem("lat").getTextContent();
			String lon = node.getAttributes().getNamedItem("lon").getTextContent();

			String ele = null;
			String time = null;

			NodeList children = node.getChildNodes();
			for (int c = 0; c < children.getLength(); c++) {
				Node child = children.item(c);
				if ("ele".equals(child.getNodeName()))
					ele = child.getTextContent();
				else if ("time".equals(child.getNodeName()))
					time = child.getTextContent();
			}


			TrackPoint trackPoint = new TrackPoint(n, lat, lon, ele, time);
			result.add(trackPoint);
		}

		return result;
	}
}

package fr.vfiack.gpxview;

import org.jcodec.api.awt.SequenceEncoder;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Let's try a merge conflict
 */
public class GpxView {
    private final Config config;
    private final Downloader downloader;
    private final File output;
    private final List<TrackPoint> points;

    private final ExecutorService executor = Executors.newFixedThreadPool(5);

    public GpxView(Config config, Downloader downloader, File output, List<TrackPoint> points) {
        this.config = config;
        this.downloader = downloader;
        this.output = output;
        this.points = points;
    }

    public void shutdown() {
        executor.shutdown();
    }

    public List<ImageContainer> getImages(List<TrackPoint> points, GpxView gpxView) throws IOException {
        List<ImageContainer> images = new ArrayList<>(points.size());
        List<TrackPoint> sequence = new Sequencer<TrackPoint>().sequence(points);

        for(TrackPoint point: sequence) {
            Future<BufferedImage> future =executor.submit(() -> {
                TrackPoint previous = (point.getIndex() > 0) ? points.get(point.getIndex() - 1) : null;

                try {
                    return gpxView.getImage(previous, point);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            });
            images.add(new ImageContainer(point, future));
        }

        images.sort((icA, icB) -> icA.getTrackPoint().getIndex() - icB.getTrackPoint().getIndex());
        return images;
    }

    private BufferedImage getImage(TrackPoint previous, TrackPoint point) throws IOException {
        File cache = new File(output, String.format("%08d.png", point.getIndex()));
        if(cache.exists()) {
            System.out.println("from cache: " + point.getIndex() );
            return ImageIO.read(cache);
        }

        System.out.println("from google: " + point.getIndex() );

        BufferedImage image = downloadImage(previous, point);
        ImageIO.write(image, "png", cache);
        return image;
    }

    private BufferedImage downloadImage(TrackPoint previous, TrackPoint point) throws IOException {
        String heading = "0";
        String pitch = "0";

        if(previous != null) {
            heading = String.valueOf(TrackPoints.getHeading(previous, point));
            pitch = String.valueOf(TrackPoints.getPitch(previous, point) / 4); //normal pitch is too high, don't know why
            //XXX might be better with pitch remaining to 0
        }

        String location = point.getLatitude() + "," + point.getLongitude();

        System.out.println("" + point.getIndex() + "/" + points.size());

        BufferedImage streetviewImg = downloader.downloadStreetView(location, pitch, heading);
        BufferedImage mapsImg = downloader.downloadMapImage(location, TrackPoints.buildMapsPath(points, point));
        BufferedImage combined = new BufferedImage(
                Math.max(config.getStreetviewWidth(), config.getMapWidth()),
                config.getStreetviewHeight() + config.getMapHeight(),
                BufferedImage.TYPE_INT_RGB);

        combined.getGraphics().drawImage(streetviewImg, 0, 0, null);
        combined.getGraphics().drawImage(mapsImg, 0, config.getStreetviewHeight(), null);
        return combined;
    }



    //--

    //stupid method to wrap checked exceptions so we can use lambdas...
    private static void encode(SequenceEncoder sequenceEncoder, BufferedImage image) {
        try {
            sequenceEncoder.encodeImage(image);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

	public static String readApiKey() {
        try (BufferedReader reader = new BufferedReader(new FileReader("api.key"))) {
            return reader.readLine();
        } catch (IOException e) {
            throw new RuntimeException("Unable to read api key: ", e);
        }
    }


	public static void main(String[] args) throws Exception {
		File gpxFile = new File("sample.gpx");
        File output = new File("output");
        output.mkdirs();

		List<TrackPoint> points = new Parser().parseGpx(gpxFile);

		String apiKey = readApiKey();
        Config config = new Config();
        Downloader downloader = new Downloader(config, apiKey);
        GpxView gpxView = new GpxView(config, downloader, output, points);

        List<ImageContainer> images = gpxView.getImages(points, gpxView);

        SequenceEncoder sequenceEncoder = new SequenceEncoder(new File(output, "gpxview.mp4"));
        images.forEach(c -> encode(sequenceEncoder, c.getImage()));
		sequenceEncoder.finish();
        System.out.println("done.");

        gpxView.shutdown();
	}
}

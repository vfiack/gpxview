package fr.vfiack.gpxview;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TrackPoints {
    public static int getHeading(TrackPoint from, TrackPoint to) {
        double fLat = Double.parseDouble(from.getLatitude());
        double fLon = Double.parseDouble(from.getLongitude());
        double tLat = Double.parseDouble(to.getLatitude());
        double tLon = Double.parseDouble(to.getLongitude());

        double diffLat = tLat - fLat;
        double diffLon = tLon - fLon;

        int angle = (int)Math.toDegrees(Math.atan2(diffLon, diffLat));
        return angle < 0 ? 360 + angle : angle;
    }

    public static int getPitch(TrackPoint from, TrackPoint to) {
        double fEle = Double.valueOf(from.getElevation());
        double tEle = Double.valueOf(to.getElevation());
        double dEle = tEle - fEle;

        if(Math.abs(dEle) < 0.001)
            return 0;

        double fLat = Double.valueOf(from.getLatitude());
        double fLon = Double.valueOf(from.getLongitude());
        double tLat = Double.valueOf(to.getLatitude());
        double tLon = Double.valueOf(to.getLongitude());

        double earthRadius = 6371000; //meters
        double dLat = Math.toRadians(tLat-fLat);
        double dLng = Math.toRadians(tLon-fLon);
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(Math.toRadians(fLat)) * Math.cos(Math.toRadians(tLat)) *
                        Math.sin(dLng/2) * Math.sin(dLng/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        float dist = (float) (earthRadius * c);


        return (int)Math.toDegrees(Math.atan(dEle / dist));
    }

    private static List<TrackPoint> reduce(List<TrackPoint> points, TrackPoint current, int size) {    	
    	int stepsCenter = 2;
    	int stepsFurther = 5;    	
    	
    	while(points.size() > size) {
        	int chunkSize = points.size() / 3;

        	//put current point in the middle of the list    
	    	int index = points.indexOf(current);
	    	int middle = points.size()/2;
	    	Collections.rotate(points, middle-index);
	
	    	//divide into 3 zones:
	    	//1: around current point; 2,3: further away    	
	    	//and remove points from all sublists, keeping more in the first area
	    	//WARNING: subList is backed by the orginal list, so changes made to the sublist
	    	//are reflected in the original. This is why we start from the end and don't need
	    	//to rebuild the resulting list
	    	
	    	List<TrackPoint> after = points.subList(middle + chunkSize/2, points.size());   	
	       	for(int i=after.size()-1;i>=0;i--) {
	    		if(i % stepsFurther != 0)
	    			after.remove(i);
	    	}
	       	
	     	List<TrackPoint> center = points.subList(middle - chunkSize/2, middle + chunkSize/2);   	
	       	for(int i=center.size()-1;i>=0;i--) {
	    		if(i % stepsCenter != 0)
	    			center.remove(i);
	    	}
	
	    	List<TrackPoint> before = points.subList(0, middle - chunkSize/2);
	    	for(int i=before.size()-1;i>=0;i--) {
	    		if(i % stepsFurther != 0)
	    			before.remove(i);
	    	}
    	}
    	
    	return points;
    }
    
    public static String buildMapsPath(List<TrackPoint> points, TrackPoint current) {
        final int MAX_POINTS = 75;

        List<TrackPoint> copy = new ArrayList<>(points);
        List<TrackPoint> reduced = reduce(copy, current, MAX_POINTS);

        StringBuilder debug = new StringBuilder();
        debug.append(current.getIndex() + "::");
        
        StringBuilder path = new StringBuilder();
        for(TrackPoint point: reduced) {
            if(point.getIndex() == current.getIndex())
                continue;

            debug.append(point.getIndex() + ",");
            
            if(path.length() > 0)
                path.append('|');

            path.append(point.getLatitude());
            path.append(',');
            path.append(point.getLongitude());
        }

        
        System.out.println(debug.toString());
        return path.toString();
    }
}

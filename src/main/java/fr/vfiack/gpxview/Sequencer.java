package fr.vfiack.gpxview;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Sequence items getting extremities first, then each time by subdividing the remaining items
 * and taking the middle.
 * It needs to take the middle of every sub-sequence iteratively, to avoid getting deep inside the first
 * half of the list before starting to sequence the second half.
 */
public class Sequencer<T> {
    private void sequenceSubLists(List<List<T>> lists, List<T> sequence) {
        List<List<T>> sublists = new ArrayList<>();

        //get the middle of each list
        for(List<T> list: lists) {
            int middle = list.size()/2;
            sequence.add(list.get(middle));

            if(middle > 0)
                sublists.add(list.subList(0, middle));

            if(middle+1 < list.size())
                sublists.add(list.subList(middle+1, list.size()));
        }

        //some date is remaining, recurse
        if(!sublists.isEmpty())
            sequenceSubLists(sublists, sequence);
    }

    public List<T> sequence(List<T> items) {
        List<T> sequence = new ArrayList<>(items.size());

        //too small, keep the order
        if(items.size() < 2) {
            sequence.addAll(items);
            return sequence;
        }

        //start from the extremities
        sequence.add(items.get(0));
        sequence.add(items.get(items.size()-1));

        //then recurse to get the middle of each block
        List<T> subList = items.subList(1, items.size()-1);
        List<List<T>> subLists = Collections.singletonList(subList);
        sequenceSubLists(subLists, sequence);

        return sequence;
    }
}
